# rubberkeys

A package manager for [rubberlocks](https://codeberg.org/potatoxel/multiplayer-godot-thing)

## Installation

- Install the Rust toolchain
- `cargo install --path .`

## Usage

See `rubberkeys help`

## Licence

See `LICENCE` file.
This project is licenced under the third version of the GNU Affero General Public Licence only.
