use std::io::Write;

use crate::{
    package::{default_project_toml, write_package_toml},
    sync::mod_dir,
    InitArgs,
};
use anyhow::Result;
use log::info;

pub fn init(opts: InitArgs) -> Result<()> {
    let path = std::env::current_dir().unwrap();
    info!("initializing new project in {}", path.to_str().unwrap());
    let name = opts
        .name
        .unwrap_or(path.file_name().unwrap().to_str().unwrap().to_string());
    std::fs::create_dir_all(mod_dir().join(&name))?;
    write_package_toml(default_project_toml(&name))?;
    if !opts.no_git {
        init_git(&name, opts.commit)?;
    }
    if !opts.no_godot {
        init_godot(&name)?;
    }
    Ok(())
}

pub fn init_git(name: &String, commit: bool) -> Result<()> {
    std::fs::File::create(".gitignore")?
        .write_fmt(format_args!("# dependencies\n/mods\n!/mods/{}", name))?;
    std::process::Command::new("git").arg("init").output()?;
    if commit {
        std::process::Command::new("git")
            .arg("add")
            .arg(".")
            .output()?;
        std::process::Command::new("git")
            .arg("commit")
            .arg("-m")
            .arg("initialize project")
            .output()?;
    }
    Ok(())
}

pub fn init_godot(name: &String) -> Result<()> {
    let export_presets = format!(
        "[preset.0]

name=\"Linux/X11\"
platform=\"Linux/X11\"
runnable=true
custom_features=\"\"
export_filter=\"scenes\"
export_files=PoolStringArray(  )
include_filter=\"mods/{}/*\"
exclude_filter=\"\"
export_path=\"\"
script_export_mode=0
script_encryption_key=\"\"

[preset.0.options]

custom_template/debug=\"\"
custom_template/release=\"\"
binary_format/64_bits=true
binary_format/embed_pck=false
texture_format/bptc=false
texture_format/s3tc=true
texture_format/etc=false
texture_format/etc2=false
texture_format/no_bptc_fallbacks=true
",
        name
    );
    std::fs::File::create("export_presets.cfg")?.write_fmt(format_args!("{}", export_presets))?;
    Ok(())
}
