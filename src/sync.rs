use crate::{
    install::mod_install,
    package::{read_package_toml, read_package_toml_of, Dependency},
};
use anyhow::{Context, Result};
use log::{debug, info};
use std::path::{Path, PathBuf};

pub fn sync() -> Result<()> {
    check_cache_links()?; // TODO relocate

    let package_toml = read_package_toml()?;
    std::fs::create_dir_all(mod_dir()).unwrap();

    for d in std::fs::read_dir(mod_dir()).unwrap() {
        let d = d.unwrap();
        let name = &d.file_name().to_str().unwrap().to_string();
        if !package_toml.dependencies.contains_key(name) && name != &package_toml.package.name {
            mod_remove(&name).context("removing unneeded mod by sync")?;
        }
    }

    for (name, details) in package_toml.dependencies {
        let dep = Dependency { name, details };
        mod_add(&dep).with_context(|| format!("adding dependency {}", dep.name))?;
    }
    info!("sync successful");
    Ok(())
}

pub fn clean_mods() -> Result<()> {
    let package_toml = read_package_toml()?;
    for d in std::fs::read_dir(mod_dir()).unwrap() {
        let d = d.unwrap();
        let name = &d.file_name().to_str().unwrap().to_string();
        if name != &package_toml.package.name {
            mod_remove(&name)?;
        }
    }
    Ok(())
}

pub fn mod_add(d: &Dependency) -> Result<()> {
    let source_loc = cache_dir().join(&d.name);
    let dest_loc = mod_dir().join(&d.name);
    if dest_loc.exists() {
        debug!("target location for {} exists, skipping", d.name);
        return Ok(());
    }
    info!("adding {}", d.name);
    if !source_loc.exists() {
        mod_install(d).with_context(|| format!("installing {}", d.name))?;
    }
    std::os::unix::fs::symlink(source_loc.join("mods").join(&d.name), dest_loc)?;

    let package_toml = read_package_toml_of(&d.name)?;
    for (name, details) in package_toml.dependencies {
        let dep = Dependency { name, details };
        mod_add(&dep)
            .with_context(|| format!("installing {} as depedency of {}", dep.name, d.name))?;
    }

    Ok(())
}

pub fn mod_remove(d: &String) -> Result<()> {
    info!("removing {}", d);
    std::fs::remove_file(mod_dir().join(&d))?;
    Ok(())
}

pub fn cache_dir() -> PathBuf {
    Path::new(&dirs::home_dir().unwrap())
        .join(".cache")
        .join("rubberkeys")
        .to_path_buf()
}
pub fn mod_dir() -> PathBuf {
    Path::new("./mods").to_path_buf()
}

pub fn check_cache_links() -> Result<()> {
    for d in std::fs::read_dir(cache_dir())? {
        let d = d?;

        if d.path().is_symlink() {
            let target = d.path().read_link()?;
            if !target.exists() {
                info!("invalid link for {:?} in cache, removing it", d);
                std::fs::remove_file(&d.path())?
            }
        }
    }
    Ok(())
}
