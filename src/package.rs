use anyhow::{Context, Result};
use serde::{Deserialize, Serialize};
use std::{
    collections::BTreeMap,
    fs::File,
    io::{Read, Write},
};

use crate::sync::cache_dir;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct PackageToml {
    pub package: Package,
    #[serde(default, serialize_with = "toml::ser::tables_last")]
    pub dependencies: BTreeMap<String, DependencyDetails>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Package {
    pub name: String,
    pub version: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct DependencyDetails {
    pub version: Option<String>,
    pub source: DependencySouce,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(untagged)]
pub enum DependencySouce {
    Path(String),
    Git(String),
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Dependency {
    pub name: String,
    pub details: DependencyDetails,
}

pub fn read_package_toml() -> Result<PackageToml> {
    let mut s = String::new();
    File::open("package.toml")?.read_to_string(&mut s)?;
    Ok(toml::from_str(s.as_str())?)
}

pub fn read_package_toml_of(name: &String) -> Result<PackageToml> {
    let mut s = String::new();
    File::open(cache_dir().join(name).join("package.toml"))
        .with_context(|| format!("{} lacks a package.toml", name))?
        .read_to_string(&mut s)?;
    Ok(toml::from_str(s.as_str())?)
}

pub fn write_package_toml(p: PackageToml) -> Result<()> {
    File::create("package.toml")?.write_fmt(format_args!("{}", toml::to_string_pretty(&p)?))?;
    Ok(())
}

pub fn default_project_toml(name: &String) -> PackageToml {
    PackageToml {
        package: Package {
            name: name.to_owned(),
            version: "0.1.0".to_string(),
        },
        dependencies: BTreeMap::new(),
    }
}
