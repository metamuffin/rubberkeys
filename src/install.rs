use std::path::Path;

use log::info;

use crate::{
    package::{Dependency, DependencySouce},
    sync::cache_dir,
};
use anyhow::Result;

pub fn mod_install(d: &Dependency) -> Result<()> {
    info!("installing {}", d.name);
    let target_path = cache_dir().join(&d.name);
    std::fs::create_dir_all(cache_dir())?;
    match &d.details.source {
        DependencySouce::Path(path) => {
            std::os::unix::fs::symlink(Path::new(&path).canonicalize()?, &target_path).unwrap();
            info!(
                "-> symlinked {} -> {}",
                path,
                cache_dir().join(&d.name).to_str().unwrap()
            );
        }
        DependencySouce::Git(url) => {
            std::fs::create_dir_all(&target_path)?;
            info!("cloning {:?} to cache", url);
            std::process::Command::new("git")
                .arg("clone")
                .arg(url)
                .arg(".")
                .current_dir(&target_path)
                .output()?;
            info!("done")
        }
    }
    Ok(())
}

pub fn mod_uninstall(_d: &Dependency) -> Result<()> {
    todo!()
}
