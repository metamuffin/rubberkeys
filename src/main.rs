pub mod init;
pub mod install;
pub mod package;
pub mod sync;

use crate::init::init;
use crate::package::{read_package_toml, DependencyDetails};
use crate::sync::sync;
use anyhow::{Context, Result};
use clap::{Args, Parser, Subcommand};
use package::{write_package_toml, DependencySouce};
use std::path::Path;
use sync::clean_mods;

#[derive(Debug, Parser)]
#[clap(about, author, version)]
pub struct CLIArgs {
    #[clap(subcommand)]
    action: Action,
}

#[derive(Debug, Subcommand)]
pub enum Action {
    /// Add a package
    Add {
        package: String,
        #[clap(short, long)]
        path: Option<String>,
        #[clap(short, long)]
        git: Option<String>,
        #[clap(short, long)]
        version: Option<String>,
    },
    /// Remove a package
    Remove { package: String },
    /// Initialize a new project in this folder
    Init {
        #[clap(flatten)]
        opts: InitArgs,
    },
    /// Initialize a new project in a new folder
    New {
        path: String,
        #[clap(flatten)]
        opts: InitArgs,
    },
    /// Show information about this project
    Info,
    /// Syncronize the mods folders content with package.toml
    Sync,
    /// Remove all (dependency) mods and clean build files
    Clean,
}

#[derive(Debug, Args)]
pub struct InitArgs {
    /// Don't initialize git
    #[clap(long)]
    no_git: bool,
    #[clap(long)]
    no_godot: bool,
    /// Override the name the package has without changing the path
    #[clap(short, long)]
    name: Option<String>,
    /// Commit the project strucuture already. Conflicts with --no-git
    #[clap(short, long)]
    commit: bool,
}

fn main() -> Result<()> {
    pretty_env_logger::formatted_builder()
        .filter_level(log::LevelFilter::Info)
        .init();
    let args = CLIArgs::parse();

    match args.action {
        Action::Add {
            package,
            git,
            path,
            version,
        } => {
            let mut p = read_package_toml()?;
            p.dependencies.insert(
                package.clone(),
                DependencyDetails {
                    version,
                    source: git.map(DependencySouce::Git).unwrap_or(
                        path.map(DependencySouce::Path)
                            .unwrap_or(DependencySouce::Path(format!("../{}", package))),
                    ),
                },
            );
            write_package_toml(p)?;
            sync().context("sync by add")?;
        }
        Action::Remove { package } => {
            let mut p = read_package_toml()?;
            p.dependencies.remove(&package);
            write_package_toml(p)?;
            sync().context("sync by remove")?;
        }
        Action::Init { opts } => init(opts)?,
        Action::New { path, opts } => {
            let p = Path::new(&path);
            std::fs::create_dir_all(p)?;
            std::env::set_current_dir(p)?;
            init(opts)?;
        }
        Action::Info => {
            println!("{:#?}", read_package_toml());
        }
        Action::Sync => sync().context("sync through cli")?,
        Action::Clean => clean_mods()?,
    };
    Ok(())
}
